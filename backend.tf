terraform{
    required_version = ">=0.12.0"
    backend "s3" {
        region = "us-east-1"
        profile = "default"
        key = "tf-statefile-bucket/terraform.tf.state"
        bucket = "tf-statefile-bucket"
    }
}